import React from 'react';
import {
  StatusBar,
} from 'react-native';

import Setup from "./src/setup";
import ReduxThunk from "redux-thunk";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "./src/lib/combineReducers";
const store = createStore(
  reducers,
  {},
  applyMiddleware(ReduxThunk)
);



const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <Provider store={store}>
        <Setup />
      </Provider>
    </>
  );
};


export default App;
