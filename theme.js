import { DefaultTheme } from 'react-native-paper';

const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
      ...DefaultTheme.colors,
      primary: '#BF360C',
      activePrimary: '#BF360C',
      secPirmary: "#C62828",
      disabledPrimary: '#E57373',
      accent: '#FF9100',
      greyBg: '#eceff1',
      greyText: '#A7AFBC',
    },
  };

  export default theme;