import mockAsyncStorage from '@react-native-community/async-storage/jest/async-storage-mock';
import mockGustureHandler from 'react-native-gesture-handler/__mocks__/RNGestureHandlerModule';
import mockReanimated from 'react-native-reanimated/mock';

jest.mock('@react-native-community/async-storage', () => mockAsyncStorage);
jest.mock('react-native-gesture-handler', () => mockGustureHandler);
jest.mock('react-native-reanimated', () => mockReanimated);
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');
jest.useFakeTimers()