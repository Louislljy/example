// components/__tests__/components/custom_component_test.ts
import React from 'react';
import renderer from 'react-test-renderer';
import { View } from "react-native";
import {
    Bullet,
    Container,
    CustomButton,
    CustomText,
    CustomTextInput,
    OtpInput,
    SubText,
    SlideButton,
    PaddedView,
    PaddedViewWithLine,
    TwoSideView,
    TwoSideViewPolar,

    CustomIcon,
    Logo,
    Loading,
    CustomCarousel,
    CustomTabbar,
} from "../../src/component";

import { SideMenuMock } from "../../src/component/sideMenu";

describe("TEST all component", () => {

    it('renders <Bullet>', () => {
        const comp = renderer
            .create(<Bullet index={1} text={"Some text"} />)
            .toJSON();
        expect(comp).toMatchSnapshot();
    });

    it('renders <CustomCarousel>', () => {
        let data = [
            {
                "image": "story/story2.jpg",
                "type": "stories",
                "title": "The Pure Brightness Day",
                "short_desc": "The Pure Brightness Day (QingMingJie), one of the 24 Seasonal Division Points",
                "date": "28 April 2020",
                "path": "story/s2.json",
                "level": "easy"
            },
            {
                "image": "story/story1.jpg",
                "type": "stories",
                "title": "The Lunar Festival",
                "short_desc": "The 15th day of the first lunar month is the traditional Lantern Festival (YuanXiaoJie)",
                "date": "23 April 2020",
                "path": "story/s1.json",
                "level": "easy"
            }
        ];

        const comp = renderer
            .create(<CustomCarousel data={data} onPress={() => {}} />)
            .toJSON();
        expect(comp).toMatchSnapshot();
    });

    describe('renders <Logo>', () => {

        it('renders <Logo> with no prop', () => {
            const comp = renderer
                .create(<Logo />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });

        it('renders <Logo> with page', () => {
            const comp = renderer
                .create(<Logo page />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });

        it('renders <Logo> with element', () => {
            const comp = renderer
                .create(<Logo element />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
    });

    it('renders <Loading>', () => {
        const comp = renderer
            .create(<Loading />)
            .toJSON();
        expect(comp).toMatchSnapshot();
    });

    describe("renders <CustomIcon>", () => {
        it('renders <CustomIcon> with no white', () => {
            const comp = renderer
                .create(<CustomIcon white onPress={() => { }} name="md-arrow-back" />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
        it('renders <CustomIcon> with color prop', () => {
            const comp = renderer
                .create(<CustomIcon color="green" onPress={() => { }} name="md-arrow-back" />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
    })

    describe("renders <Container>", () => {
        it('renders <Container> with no props', () => {
            const comp = renderer
                .create(<Container />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
        it('renders <Container> with scrollable', () => {
            const comp = renderer
                .create(<Container scrollable />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
        it('renders <Container> with padded', () => {
            const comp = renderer
                .create(<Container padded />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
        it('renders <Container> with scrollable and padded', () => {
            const comp = renderer
                .create(<Container scrollable padded />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
    })


    it('renders <CustomText>', () => {
        const comp = renderer
            .create(<CustomText>test</CustomText>)
            .toJSON();
        expect(comp).toMatchSnapshot();
    });

    describe('renders <CustomButton>', () => {
        it('renders <CustomButton> with no props', () => {
            const comp = renderer
                .create(<CustomButton>test</CustomButton>)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
        it('renders <CustomButton> with text', () => {
            const comp = renderer
                .create(<CustomButton text={"test"} />)
                .toJSON();
            expect(comp).toMatchSnapshot();
        });
    });


    it('renders <CustomTextInput>', () => {
        const comp = renderer
            .create(
                <CustomTextInput
                    label='test'
                    onChangeText={(text) => { }}
                    value={""}
                />
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    });

    it("renders <Otpinput>", () => {
        const comp = renderer
            .create(
                <OtpInput
                    onCodeChanged={() => { }}
                    onCodeFilled={() => { }}
                    onReset={() => { }}
                    autoFocusOnLoad
                />
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <SubText>", () => {
        const comp = renderer
            .create(
                <SubText>Test</SubText>
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <SlideButton>", () => {
        const comp = renderer
            .create(
                <SlideButton.SlideButton
                    onSlideSuccess={() => { }}
                    slideDirection={SlideButton.SlideDirection.LEFT}
                    width={500}
                    height={50}>
                    <View style={{ height: 50, width: 500 }}>
                        <CustomText>Slide Button</CustomText>
                    </View>
                </SlideButton.SlideButton >
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <PaddedView>", () => {
        const comp = renderer
            .create(
                <PaddedView />
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <PaddedViewWithLine>", () => {
        const comp = renderer
            .create(
                <PaddedViewWithLine />
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <TwoSideView>", () => {
        const comp = renderer
            .create(
                <TwoSideView />
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <TwoSideViewPolar>", () => {
        const comp = renderer
            .create(
                <TwoSideViewPolar />
            )
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

    it("renders <SideMenu>", () => {
        const comp = renderer
            .create(<SideMenuMock />)
            .toJSON();
        expect(comp).toMatchSnapshot();
    })

})
