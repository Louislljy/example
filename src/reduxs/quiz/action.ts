

import Quiz from "../../services/quiz";

export const getType = {
}

export const getActions = {
}

export const getJSON = (path: string): Function => {
    return (dispatch: Function) => {
        return Quiz.getJSON(path).then(rslt => {
            return rslt;
        }).catch(err => {
            throw err;
        })
    }
};
