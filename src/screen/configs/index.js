import React from 'react';
import { connect } from "react-redux";
import { withTheme } from "react-native-paper";
import { triggerLoad } from "../../lib/commonAction";
import { getJSON } from "../../reduxs/quiz/action";

class Configs extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: []
        };
        this.init();
    }

    init = () => {
        this.props.getJSON("settings.json").then(rslt => {
            this.setState({ data: rslt })
        })
    }

    render = () => {
        return (
            <></>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        triggerLoad: (bool) => dispatch(triggerLoad(bool)),
        getJSON: (path) => dispatch(getJSON(path)),
    };
}

export default connect(null, mapDispatchToProps)(withTheme(Configs));