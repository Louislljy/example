import React from "react";
import { Container } from "../../component";
import { withTheme } from "react-native-paper";

import { connect } from "react-redux";
import { triggerLoad, toggleModal } from "../../lib/commonAction";
import { getJSON } from "../../reduxs/quiz/action";

class Story extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            stories: [],
        };
        this.init();
    }

    init() {
        // this.props.triggerLoad(true);
        let arr = [this.getStories()];
        return Promise.all(arr).then(rslt => {
            // this.props.triggerLoad(false);
        })
    }

    getStories = () => {
        return this.props.getJSON("stories.json").then(rslt => {
            this.setState({ stories: rslt })
        })
    }

    render() {
        return (
            <Container scrollable tabVisible>
            </Container>
        );
    }

}

const mapStateToProps = (state) => {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {
        triggerLoad: (load) => dispatch(triggerLoad(load)),
        toggleModal: (toggle) => dispatch(toggleModal(toggle)),
        getJSON: (path) => dispatch(getJSON(path)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Story));