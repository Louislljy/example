import React from "react";
import { Container, CustomCarousel, CustomText, PaddedView, Loading } from "../../component";
import { withTheme } from "react-native-paper";
import { connect } from "react-redux";
import { triggerLoad, toggleModal } from "../../lib/commonAction";
import { getJSON } from "../../reduxs/quiz/action";

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            data: [],
            stories: [],
        };
        this.init();
    }

    init = () => {
        // this.props.triggerLoad(true);
        let arr = [this.getActivities(), this.getStories()];
        return Promise.all(arr).then(rslt => {
            // this.props.triggerLoad(false);
        })
    }

    getStories = () => {
        return this.props.getJSON("stories.json").then(rslt => {
            this.setState({ stories: rslt })
        })
    }

    getActivities = () => {
        return this.props.getJSON("curri.json").then(rslt => {
            this.setState({ data: rslt })
        })
    }

    componentReady = (type) => {
        return this.state[type].length > 0 ? this.selective(type) : <Loading type="element" />
    }

    selective = (type) => {
        if (type == "stories") return <CustomCarousel data={this.state.stories.slice(0, 5)} onPress={this.goStory} />
    }

    render() {
        return (
            <Container scrollable tabVisible>
                <PaddedView style={{ flex: 1 }}>
                    <CustomText header2 style={{ paddingLeft: 10, paddingBottom: 5 }}>{`Readings`}</CustomText>
                </PaddedView>
            </Container>
        );
    }

}

const mapStateToProps = (state) => {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
        triggerLoad: (load) => dispatch(triggerLoad(load)),
        toggleModal: (toggle) => dispatch(toggleModal(toggle)),
        getJSON: (path) => dispatch(getJSON(path)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Home));