/* eslint-disable react/display-name */

import React from 'react';
import { View } from "react-native";

import MainNav from "./navigation/mainNavigator";

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

const AppNav = createStackNavigator({
    // AuthNav: { screen: AuthNav },
    Dashboard: { screen: MainNav },
},
    {
        initialRouteName: 'Dashboard',
        headerMode: 'none',
    }
);

const App = createAppContainer(AppNav);

export default () => {

    return (
        <View style={{ flex: 1 }}>
            <App />
        </View>
    );

};