export const getType = {
    START_SPIN: "START_SPIN",
    SHOW_MSG: "SHOW_MSG",
    CLOSE_MSG: "CLOSE_MSG",
    TOGGLE_MODAL: "TOGGLE_MODAL",
    STORE_NOTIFICATION_KEY: "STORE_NOTIFICATION_KEY",
}

export const getActions = {
    triggerLoad: (load: boolean) => { return { type: getType.START_SPIN, payload: load } },
    showMsg: (type: string, title?: string, msg?: string) => {
        return { type: getType.SHOW_MSG, payload: { type: type, title: title, msg: msg } }
    },
    closeMsg: () => { return { type: getType.CLOSE_MSG } },
    toggleModal: (toggle: boolean) => { return { type: getType.TOGGLE_MODAL, payload: toggle } },
    saveNotificationKey: (notification_key: string) => { return { type: getType.STORE_NOTIFICATION_KEY, payload: notification_key } },
    // toggleModal: () => { return { type: getType.TOGGLE_MODAL } }
}

export const closeDialog = () => {
    return (dispatch: Function) => {
        dispatch(getActions.closeMsg());
    }
};

export const triggerLoad = (load: boolean, dispatch: Function) => {
    return (dispatch: Function) => {
        dispatch(getActions.triggerLoad(load));
    }
};

// when this action is active, for reducer, action function do 
// return: action type and action data.
export const toggleModal = (toggle: boolean, dispatch: Function) => {
    return (dispatch: Function) => {
        dispatch(getActions.toggleModal(toggle));
    }
}
export function showModal(flag: boolean) {
    return {
        type: getType.TOGGLE_MODAL,
        flag
    }
}

export const saveNotificationKey = (notification_key : string) => {
    return (dispatch: Function) => {
        dispatch(getActions.saveNotificationKey(notification_key));
    }
};

export const invalidForm = (msg: string): Function => {
    return (dispatch: Function) => {
        dispatch(getActions.showMsg("error", "Error", msg))
    }
};

export const showMessage = (msg: string): Function => {
    return (dispatch: Function) => {
        dispatch(getActions.showMsg("success", "Success", msg))
    }
};

