import { combineReducers } from "redux";
import * as common from "../lib/commonRedux";
import * as quiz from "../reduxs/quiz/redux";

const arr = [
    common,
    quiz,
];

let reducers = {};

for (let item in arr){
    Object.assign(reducers, arr[item].default);
}

export default combineReducers(
    reducers
);
