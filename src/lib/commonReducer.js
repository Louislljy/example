import { getType as commonType } from "./commonAction";

export const Common = (state = {
    "loading": false,
    "showMsg": false,
    "msgType": "",
    "msgTitle": "",
    "msgDetail": "",
    "toggle": false,
    "notificsation_key": "",
}, action) => {
    switch (action.type) {
        case commonType.START_SPIN:
            return {
                ...state,
                loading: action.payload
            }
        case commonType.SHOW_MSG:
            return {
                ...state,
                showMsg: true,
                msgType: action.payload.type,
                msgTitle: action.payload.title ? action.payload.title : "",
                msgDetail: action.payload.msg ? action.payload.msg : "",
            }
        case commonType.CLOSE_MSG:
            return {
                ...state,
                showMsg: false,
                msgType: "",
                msgTitle: "",
                msgDetail: "",
            }
        case commonType.TOGGLE_MODAL:
            return {
                ...state,
                toggle: action.payload,
            }
        // case commonType.UNTOGGLE_MODAL:
        //     return {
        //         modalType: null,
        //         modalProps: {},
        case commonType.STORE_NOTIFICATION_KEY:
            return {
                ...state,
                notificsation_key: action.payload,
            }
        // case commonType.TOGGLE_MODAL:
        // return {
        //         ...state,
        //     }
        default:
            return state;
    }

}
