export const nameExtractor = (name) => {
    let initials = name.match(/\b\w/g) || [];
    initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
    return initials;
}

export const autoCap = (lower) => {
    if(!lower){
        return null;
    }
    return lower.charAt(0).toUpperCase() + lower.substring(1);
}

export const generateID = () => {
    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
    return randomString(8, '0123456789abcdefghijklmnopqrstuvwxyz');
}

export const formatTime = (datestring, dateOnly) => {
    let date = new Date(datestring * 1000);
    let monthNames = [
        "JAN", "FEB", "MAR",
        "APR", "MAY", "JUN", "JUL",
        "AUG", "SEP", "OCT",
        "NOV", "DEC"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;

    return day + ' ' + monthNames[monthIndex] + ' ' + year + (dateOnly ? "" : ", " + hours + ':' + minutes + ' ' + ampm);
}

export const formatCompareDate = (datestring, dateOnly) => {
    let date = new Date(datestring * 1000);

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + (monthIndex + 1) + ' ' + year;
}

export const formatDate = (date) => {
    let monthNames = [
        "JAN", "FEB", "MAR",
        "APR", "MAY", "JUN", "JUL",
        "AUG", "SEP", "OCT",
        "NOV", "DEC"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


import Animated from "react-native-reanimated";

const { round, interpolate, Extrapolate, color } = Animated;

const colorRegex = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;

const hexToRgb = (hex) => {
  const result = colorRegex.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null;
};

const white = { r: 255, g: 255, b: 255 };

export const interpolateColors = (
  animationValue,
  inputRange,
  hexColors,
) => {
  const colors = hexColors.map(hexColor => hexToRgb(hexColor) || white);
  const r = round(
    interpolate(animationValue, {
      inputRange,
      outputRange: colors.map(c => c.r),
      extrapolate: Extrapolate.CLAMP,
    }),
  );
  const g = round(
    interpolate(animationValue, {
      inputRange,
      outputRange: colors.map(c => c.g),
      extrapolate: Extrapolate.CLAMP,
    }),
  );
  const b = round(
    interpolate(animationValue, {
      inputRange,
      outputRange: colors.map(c => c.b),
      extrapolate: Extrapolate.CLAMP,
    }),
  );
  return color(r, g, b);
};