import API from "../lib/api";
export default class Quiz {

    static getJSON(path: string) {
        return API.get(path).then(rslt => {
            return rslt;
        }).catch(err => {
            throw "ERR : " + err.toString();
        })
    }

}