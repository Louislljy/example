import React from 'react';
import Home from "../screen/home";
import Story from "../screen/story";
import Configs from "../screen/configs";
import { RFValue } from 'react-native-responsive-fontsize';
import theme from "../../theme";

const headerCommonStyle = {
    backgroundColor: "transparenty",
    shadowOpacity: 0,
    elevation: 0,
}

const headerTitle = {
    fontSize: RFValue(20),
    fontWeight: "bold",
    borderBottomColor: theme.colors.accent,
    borderBottomWidth: 2,
    color: theme.colors.primary
}

const Settings = {
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            headerStyle: headerCommonStyle,
            headerTitleStyle: headerTitle
        }),
    },
    Story: {
        screen: Story,
        navigationOptions: ({ navigation }) => ({
            headerStyle: headerCommonStyle,
            headerTitleStyle: headerTitle
        }),
    },
    Configs: {
        screen: Configs,
        navigationOptions: ({ navigation }) => ({
            headerStyle: headerCommonStyle,
            headerTitleStyle: headerTitle
        }),
    },
};


export default Settings;