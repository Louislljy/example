import React from 'react';
import Settings from "../screenSettings";
import { createStackNavigator } from 'react-navigation-stack';
import { RFValue } from 'react-native-responsive-fontsize';

export default (initial) => {
    let temp = Settings;
    let obj = {};
    // use this exceptional to ensure the app will not remder the following page into stack
    // to ensure this only exist on tab
    let exceptional = [
        "Story"
    ]
    if (exceptional.indexOf(initial) == -1) {
        for (let item in temp) {
            if (exceptional.indexOf(item) == -1) {
                obj[item] = temp[item];
            }
        }
    } else {
        exceptional.splice(exceptional.indexOf(initial), 1);
        for (let item in temp) {
            if (exceptional.indexOf(item) == -1) {
                obj[item] = temp[item];
            }
        }
    }

    return createStackNavigator(obj, {
        initialRouteName: initial,
        defaultNavigationOptions: ({ navigation }) => ({
            headerStyle: {
                shadowOpacity: 0,
                elevation: 0,
                textAlign: "center",
            },
            headerTitleStyle: {
                fontSize: RFValue(20),
                textAlign: 'center',
                flexGrow: 1,
                alignSelf: 'center',
                fontWeight: "bold"
            },
        }),
    }
    );
};