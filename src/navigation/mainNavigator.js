import React from 'react';

import { createBottomTabNavigator } from 'react-navigation-tabs';
import FindNav from "./tabNavScreen/findJobsNav";
import Icon from "react-native-vector-icons/Feather";
import theme from "../../theme";
import { CustomTabbar } from "../component";

// use this to remove tab for certain page
const noTab = []

const TabScreens = createBottomTabNavigator(
  {
    "Stories": {
      screen: FindNav("Story"),
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon style={{ fontSize: 20, color: tintColor }} name="book" />
        )
      }
    },
    "Home": {
      screen: FindNav("Home"),
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon style={{ fontSize: 20, color: tintColor }} name="home" />
        )
      }
    },
    "Configs": {
      screen: FindNav("Configs"),
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon style={{ fontSize: 20, color: tintColor }} name="package" />
        )
      }
    }
  },
  {
    initialRouteName: "Home",
    tabBarOptions: {
      activeTintColor: theme.colors.primary,
      inactiveTintColor: theme.colors.greyText,
      keyboardHidesTabBar: true,
      tabStyle: {
        borderTopColor: theme.colors.greyText,
        borderTopWidth: 0.5
      }
    },
    tabBarComponent: (props) => {
      return <CustomTabbar {...props} />
    },
    defaultNavigationOptions: ({ navigation }) => {
      let tabBarVisible = true;
      let swipeEnabled = true;
      if (navigation.state.index > 0 && noTab.indexOf(navigation.state.routes[1].routeName) > -1) {
        tabBarVisible = false;
        swipeEnabled = false;
      }
      return {
        tabBarVisible,
        swipeEnabled
      };
    }
  }
);

export default TabScreens;