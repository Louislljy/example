import React from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import { Snackbar } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import { closeDialog, saveNotificationKey } from "./lib/commonAction";
import Nav from "./navigator";
import SplashScreen from 'react-native-splash-screen'
import messaging from '@react-native-firebase/messaging'
import NotificationPopup from 'react-native-push-notification-popup';

class Setup extends React.Component {

    async componentDidMount() {
        SplashScreen.hide();

        messaging().onNotificationOpenedApp(remoteMessage => {
            // console.log(
            //     'Notification caused app to open from background state:',
            //     remoteMessage
            // );
        });

        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                // console.log(
                //     'Notification caused app to open from quit state:',
                // );
            });


        messaging().onMessage(async message => {
            // console.log("3333333333333333333", message)
            this.displayNotification(message.notification);
        });

        messaging().setBackgroundMessageHandler(async remoteMessage => {
            // console.log('Message handled in the background!', remoteMessage);
        });

        if (messaging().hasPermission()) {
            messaging().requestPermission().then(answer => {
                if (answer) {
                    // console.log("get permission")
                } else {
                    alert("Failed to grant notification permission");
                }
            });
        } else {
            alert("Failed to grant notification permission");

        }

        messaging().getToken().then(token => {
            try {
                // console.log("dont know what token", token);
            } catch (error) {
                // console.log("dont know why error", error);
            }
        });

    }

    displayNotification(msg) {
        console.log(msg);
        this.popup.show({
            onPress: function () { console.log('Pressed') },
            appIconSource: require('../assets/icon.jpg'),
            appTitle: 'example',
            timeText: 'Now',
            title: msg.title,
            body: msg.body,
            slideOutTime: 5000
        });
    }

    renderMsg(type) {
        if (type == "error") {
            return "red";
        } else if (type == "success") {
            return "#81C784";
        } else {
            return "black";
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Spinner
                    visible={this.props.loading}
                />
                <Snackbar
                    visible={this.props.showMsg}
                    onDismiss={this.props.closeDialog}
                    duration={1000}
                    style={{ backgroundColor: this.renderMsg(this.props.msgType) }}
                >
                    {this.props.msgDetail}
                </Snackbar>
                <Nav parent={this.props.navigation} />
                <NotificationPopup ref={ref => this.popup = ref} />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.Common.loading,
        showMsg: state.Common.showMsg,
        msgType: state.Common.msgType,
        msgTitle: state.Common.msgTitle,
        msgDetail: state.Common.msgDetail,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        closeDialog: () => dispatch(closeDialog()),
        saveNotificationKey: (notification_key) => dispatch(saveNotificationKey(notification_key)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Setup);
