import React from "react";
import { withTheme, TextInput } from "react-native-paper";
import { RFValue } from "react-native-responsive-fontsize";

class CustomTextInput extends React.PureComponent {

    render() {
        return (
            <TextInput
                {...this.props}
                tintColor={this.props.theme.colors.greyText}
                baseColor={this.props.theme.colors.greyText}
                fontSize={this.props.fontSize ? RFValue(this.props.fontSize) : RFValue(12)}
                labelFontSize={this.props.labelFontSize ? RFValue(this.props.labelFontSize) : RFValue(12)}
                titleFontSize={this.props.titleFontSize ? RFValue(this.props.titleFontSize) : RFValue(12)}
                inputContainerStyle={{
                    // height: 75,
                    // justifyContent: "center",
                    // marginBottom: 5
                }}
            />
        );
    }

}

export default withTheme(CustomTextInput)
