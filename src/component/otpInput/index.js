import React from "react";
import { withTheme } from "react-native-paper";
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { RFValue } from "react-native-responsive-fontsize";
import CustomText from "../text";
import styled from "styled-components/native";

const OTPContainer = styled.View`
    height: 100px;
    flex: 1;
    flex-direction: row;
    margin-top: 50px;
`;

const OTPLeft = styled.View`
    flex: 7;
`;

const OTPRight = styled.View`
    flex: 3;
    height: 200px;
    padding-left: 10px;
`;

class OtpInput extends React.PureComponent {

    render() {
        return (
            <OTPContainer>
                <OTPLeft>
                    <OTPInputView
                        style={{ width: '100%', height: 25 }}
                        pinCount={4}
                        code={this.props.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                        onCodeChanged={this.props.onCodeChanged}
                        autoFocusOnLoad
                        codeInputFieldStyle={{
                            borderWidth: 0,
                            borderBottomColor: this.props.theme.colors.greyText,
                            borderBottomWidth: 1,
                        }}
                        codeInputHighlightStyle={{
                            borderWidth: 0,
                            borderBottomColor: this.props.theme.colors.greyText,
                            borderBottomWidth: 2,
                        }}
                        onCodeFilled={this.props.onCodeFilled}
                    />
                </OTPLeft>
                <OTPRight>
                    <CustomText
                        style={{
                            fontSize: RFValue(10)
                        }}
                    >
                        {"Didn't receive it?"}
                    </CustomText>
                    <CustomText
                        style={{
                            fontSize: RFValue(10)
                        }}
                        onPress={this.props.onReset}
                    >
                        {"Resend OTP"}
                    </CustomText>
                </OTPRight>
            </OTPContainer>
        );
    }

}

export default (withTheme(OtpInput));