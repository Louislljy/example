import React from 'react';
import { PaddedViewWithLine, PaddedView } from '..';
import Container from '../container';
import CustomText from '../text';
import CustomButton from '../button';
import { List } from "react-native-paper";
import { ScrollView } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { RFValue } from 'react-native-responsive-fontsize';
import { withTheme } from "react-native-paper";
import { connect } from "react-redux";
import { triggerLoad } from "../../lib/commonAction";
import { requestLogout } from "../../reduxs/quiz/action";

export class SideMenu extends React.Component {

  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout() {
    this.props.triggerLoad(true);
    this.props.requestLogout(this.props.notificsation_key).then(() => {
      this.props.triggerLoad(false);
    });
  }

  render() {
    return (
      <Container>
        <ScrollView showsVerticalScrollIndicator={false}>
          <PaddedViewWithLine
            style={{
              paddingTop: 5,
              paddingBottom: 15,
            }}
          >
            <List.Item
              title={<CustomText header3>{"Louis Loh"}</CustomText>}
              style={{
                justifyContent: "center", alignItems: "center"
              }}
              description={"Some text"}
              titleStyle={{ fontWeight: "bold", marginLeft: 13, marginBottom: 8, marginTop: 13 }}
              descriptionStyle={{ marginLeft: 15, lineHeight: 25, fontSize: RFValue(12) }}
              onPress={() => { this.props.navigation.navigate("Profile") }}
            />
          </PaddedViewWithLine>

          <PaddedViewWithLine
            style={{
              paddingTop: 10,
              paddingBottom: 10,
            }}
          >
            <List.Item
              title={<CustomText>My Profile</CustomText>}
              style={{ padding: 0 }}
              titleStyle={{ fontWeight: "bold" }}
              onPress={() => { this.props.navigation.navigate("Profile") }}
            />
            <List.Item
              title={<CustomText>My Messages</CustomText>}
              style={{ padding: 0 }}
              titleStyle={{ fontWeight: "bold" }}
              onPress={() => { this.props.navigation.navigate("Tab5") }}
            />
          </PaddedViewWithLine>
          <PaddedViewWithLine
            style={{
              paddingTop: 10,
              paddingBottom: 10,
            }}
          >
            <List.Item
              title={<CustomText>General</CustomText>}
              style={{ padding: 0 }}
              titleStyle={{ fontWeight: "bold" }}
              onPress={() => this.props.navigation.navigate("Sites", { from: "general", title: "General" })}
            />
            <List.Item
              title={<CustomText infoText>Help Centre</CustomText>}
              style={{ padding: 0 }}
              titleStyle={{ fontWeight: "bold" }}
              onPress={() => this.props.navigation.navigate("Sites", { from: "help_centre", title: "Help Centre" })}
            />
            <List.Item
              title={<CustomText infoText>FAQs</CustomText>}
              style={{ padding: 0 }}
              titleStyle={{ fontWeight: "bold" }}
              onPress={() => this.props.navigation.navigate("Sites", { from: "faq", title: "FAQs" })}
            />
            <List.Item
              title={<CustomText infoText>Terms & Privacy Policy</CustomText>}
              style={{ padding: 0 }}
              titleStyle={{ fontWeight: "bold" }}
              onPress={() => this.props.navigation.navigate("Sites", { from: "term_privacy", title: "Terms & Privacy Policy" })}
            />
          </PaddedViewWithLine>
        </ScrollView>

        <PaddedView
          style={{
            paddingTop: 5,
            paddingBottom: 5,
            flexDirection: "row",
            justifyContent: "space-between",
            borderTopWidth: 0.25,
            borderTopColor: this.props.theme.colors.greyText,
          }}
        >
          <CustomButton
            primary
            onPress={this.logout}
          >
            <Icon style={{ fontSize: RFValue(20) }} name="log-out" /><CustomText style={{ color: this.props.theme.colors.primary }}>{" "}Log out</CustomText>
          </CustomButton>
        </PaddedView>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestLogout: (push_notification_key) => dispatch(requestLogout(push_notification_key)),
    triggerLoad: (load) => dispatch(triggerLoad(load)),
  };
}

export const SideMenuMock = withTheme(SideMenu);
export default connect(mapStateToProps, mapDispatchToProps)(withTheme(SideMenu));