import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from './sliderStyle';
import { IMAGE_URL } from "../../lib/api";

export default class SliderEntry extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image() {
        const { data: { image }, parallax, parallaxProps, even } = this.props;

        return parallax ? (
            <ParallaxImage
                source={{ uri: IMAGE_URL + image }}
                containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
                style={styles.image}
                parallaxFactor={0.35}
                showSpinner={true}
                spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
                {...parallaxProps}
            />
        ) : (
                <Image
                    source={{ uri: IMAGE_URL + image }}
                    style={styles.image}
                />
            );
    }

    chooseColor(level){
        if(level == "easy"){
            return "green";
        }

        return "black"
    }

    render() {
        const { data: { title, short_desc, date, level }, even } = this.props;
        // console.log(">>>", level)
        const uppercaseTitle = title ? (
            <Text
                style={[styles.title, even ? styles.titleEven : {}]}
                numberOfLines={2}
            >
                {title.toUpperCase()}
            </Text>
        ) : false;

        return (
            <TouchableOpacity
                activeOpacity={1}
                style={[styles.slideInnerContainer, { width: this.props.width ? this.props.width : null }]}
                onPress={this.props.onPress}
            >
                <View style={styles.shadow} />
                <View style={[styles.imageContainer, even ? styles.imageContainerEven : {}]}>
                    {this.image}
                    <View style={[styles.radiusMask, even ? styles.radiusMaskEven : {}]} />
                </View>
                <View style={[styles.textContainer, even ? styles.textContainerEven : {}]}>


                    <Text>
                        <Text
                            style={{ color: this.chooseColor(level), fontWeight: "bold", textAlign: "right" }}
                            numberOfLines={1}
                        >
                            {"(" + level.toUpperCase() + ")  "}
                        </Text>
                        {uppercaseTitle}
                    </Text>
                    <Text
                        style={[styles.subtitle, even ? styles.subtitleEven : {}]}
                        numberOfLines={2}
                    >
                        {short_desc}
                    </Text>
                    <Text
                        style={[styles.subtitle, even ? styles.subtitleEven : {}]}
                        numberOfLines={1}
                    >
                        {date}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}