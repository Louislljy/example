import React from "react";
import Carousel from 'react-native-snap-carousel';
import SliderEntry from "./SliderEntry";
import styles from "./styles";
import { sliderWidth, itemWidth } from './sliderStyle';

export default class CustomCarousel extends React.PureComponent {

    constructor(props){
        super(props);
        this._renderItem = this._renderItem.bind(this);
    }

    _renderItem({ item, index }) {
        return <SliderEntry data={item} even={true} onPress={() => this.props.onPress(item.path, item.title)}/>;
    }

    render() {
        return (
            <>
                <Carousel
                    data={this.props.data}
                    renderItem={this._renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    inactiveSlideScale={0.95}
                    inactiveSlideOpacity={1}
                    enableMomentum={true}
                    activeSlideAlignment={'start'}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    activeAnimationType={'spring'}
                    activeAnimationOptions={{
                        friction: 4,
                        tension: 40
                    }}
                />
            </>

        );
    }
}
