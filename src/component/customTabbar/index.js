import React from "react";
import { Dimensions, View, StyleSheet, TouchableWithoutFeedback, Animated } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import theme from "../../../theme";
import Svg, { Path } from "react-native-svg";
import * as shape from "d3-shape";
const { width } = Dimensions.get("window");
const height = 50;

const AnimatedSvg = Animated.createAnimatedComponent(Svg);

class CustomTabbar extends React.PureComponent {

    constructor(props) {
        super(props);
        this.tabs = props.navigation.state.routes;
        this.tabWidth = (width / this.tabs.length);

        this.left = shape.line().x(d => d.x).y(d => d.y)([
            { x: 0, y: 0 },
            { x: width, y: 0 },
        ]);
        this.tab = shape.line().x(d => d.x).y(d => d.y).curve(shape.curveBasis)([
            { x: width, y: 0 },
            { x: width + 5, y: 0 },
            { x: width + 10, y: 10 },
            { x: width + 15, y: height - 10 },
            { x: width + this.tabWidth - 15, y: height - 10 },
            { x: width + this.tabWidth - 10, y: 10 },
            { x: width + this.tabWidth - 5, y: 0 },
            { x: width + this.tabWidth, y: 0 },
        ]);
        this.right = shape.line().x(d => d.x).y(d => d.y)([
            { x: width + this.tabWidth, y: 0 },
            { x: width * 2, y: 0 },
            { x: width * 2, y: height },
            { x: 0, y: height },
            { x: 0, y: 0 },
        ]);
        this.d = `${this.left} ${this.tab} ${this.right}`;
        this.value = new Animated.Value(this.tabWidth * props.navigation.state.index);
        this.values = this.tabs.map((tab, index) => new Animated.Value(index === props.navigation.state.index ? 1 : 0));
        this.onPress = this.onPress.bind(this);
    }

    onPress = (index, tab) => {
        // do other stuff like tap second time to move to first page
        // do other stull like tap to move scrollup
        // etc 
        const tabWidth = width / this.tabs.length;
        this.props.onTabPress({ route: tab });
        Animated.sequence([
            Animated.parallel(
                this.values.map(v => Animated.timing(v, {
                    toValue: 0,
                    duration: 100,
                    useNativeDriver: true,
                })),
            ),
            Animated.parallel([
                Animated.spring(this.value, {
                    toValue: this.tabWidth * index,
                    useNativeDriver: true,
                }),
                Animated.spring(this.values[index], {
                    toValue: 1,
                    useNativeDriver: true,
                }),
            ]),
        ]).start();
    }

    render() {

        const { value } = this;
        const translateX = value.interpolate({
            inputRange: [0, width],
            outputRange: [-width, 0],
        });

        return (
            <View style={{
                backgroundColor: 'rgba(0, 0, 0, 0)',
                position: 'absolute',
                left: 0,
                right: 0,
                bottom: 0,
            }}>
                <AnimatedSvg width={width * 2} {...{ height }} style={{ transform: [{ translateX }] }}>
                    <Path stroke={this.props.activeTintColor} fill={"white"} d={this.d} />
                </AnimatedSvg>
                <View style={StyleSheet.absoluteFill}>
                    <View style={{ flexDirection: "row", }}>
                        {
                            this.tabs.map((tab, key) => {
                                const tabWidth = width / this.tabs.length;
                                const cursor = tabWidth * key;
                                const opacity = value.interpolate({
                                    inputRange: [cursor - tabWidth, cursor, cursor + tabWidth],
                                    outputRange: [1, 0, 1],
                                    extrapolate: "clamp",
                                });
                                const translateY = this.values[key].interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [height, 0],
                                    extrapolate: "clamp",
                                });
                                const opacity1 = this.values[key].interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0, 1],
                                    extrapolate: "clamp",
                                });
                                return (
                                    <>
                                        <TouchableWithoutFeedback onPress={() => this.onPress(key, tab)}>
                                            <Animated.View style={{
                                                flex: 1,
                                                justifyContent: "center",
                                                alignItems: "center",
                                                height: height,
                                                opacity: opacity
                                            }}>
                                                {this.props.renderIcon({ route: tab, focused: false, tintColor: this.props.inactiveTintColor })}
                                            </Animated.View>

                                        </TouchableWithoutFeedback>
                                        <Animated.View
                                            style={{
                                                position: "absolute",
                                                top: -20,
                                                left: tabWidth * key,
                                                width: tabWidth,
                                                height: height,
                                                justifyContent: "center",
                                                alignItems: "center",
                                                opacity: opacity1,
                                                transform: [{ translateY }],
                                            }}
                                        >
                                            <View style={{
                                                backgroundColor: this.props.activeTintColor,
                                                width: 50,
                                                height: 50,
                                                borderRadius: 500,
                                                justifyContent: "center",
                                                alignItems: "center",
                                            }}>
                                                {/* <Icon name={tab.key} color={"white"} size={25} /> */}
                                                {this.props.renderIcon({ route: tab, focused: true, tintColor: "white" })}
                                            </View>
                                        </Animated.View>
                                    </>
                                );
                            })
                        }
                    </View>
                </View>
            </View>
        );
    }
}

export default CustomTabbar;