import React from "react";
import { Text, withTheme } from "react-native-paper";
import { RFValue } from "react-native-responsive-fontsize";
import PropTypes from "prop-types";

class CustomText extends React.PureComponent {

    getStyle(props) {
        let obj = {
            // fontSize: RFValue(16),
            fontSize: RFValue(14),
            lineHeight: 25,
        }

        if (this.props.style && this.props.style.fontSize) {
            obj = {
                ...obj,
                fontSize: RFValue(this.props.style.fontSize),
            }
        }
        if (this.props.primary) {
            obj = {
                ...obj,
                color: props.theme.colors.primary,
            }
        }

        if (this.props.bg) {
            obj = {
                ...obj,
                backgroundColor: "white",
            }
        }

        if (this.props.header) {
            obj = {
                ...obj,
                fontSize: RFValue(30),
                lineHeight: RFValue(30),
                fontWeight: "bold",
                paddingTop: 10,
                marginBottom: 15
            }
        }

        if (this.props.header2) {
            obj = {
                ...obj,
                fontSize: RFValue(20),
                lineHeight: RFValue(20),
                fontWeight: "bold",
                paddingTop: 10,
                marginBottom: 15
            }
        }

        if (this.props.header3) {
            obj = {
                ...obj,
                fontSize: RFValue(16),
                lineHeight: RFValue(16),
                fontWeight: "bold",
                paddingTop: 10,
                marginBottom: 15
            }
        }

        if (this.props.header4) {
            obj = {
                ...obj,
                fontSize: RFValue(20),
                fontWeight: "bold",
                paddingTop: 10,

            }
        }

        if (this.props.title) {
            obj = {
                ...obj,
                fontSize: RFValue(20),
            }
        }

        if (this.props.subtitle) {
            obj = {
                ...obj,
                fontSize: RFValue(16),
            }
        }

        if (this.props.des) {
            obj = {
                ...obj,
                fontSize: RFValue(11),
                lineHeight: 20,
            }
        }
       
        if (this.props.bordertitle) {
            obj = {
                ...obj,
                borderBottomWidth: 1,
                borderBottomColor: props.theme.colors.primary,
                paddingBottom: 10,
                paddingLeft: 20,
                paddingRight: 20,
            }
        }
        if (this.props.bold) {
            obj = {
                ...obj,
                fontWeight: "bold",
            }
        }

        if (this.props.infoText) {
            obj = {
                ...obj,
                color: props.theme.colors.greyText
            }
        }

        if (this.props.onPress) {
            obj = {
                ...obj,
                color: props.theme.colors.accent
            }
        }
        return obj
    }

    render() {
        return (
            <Text
                allowFontScaling={false}
                
                {...this.props}
                style={{
                    ...this.props.style,
                    ...this.getStyle(this.props),
                }}
            >
                {this.props.children}
            </Text>
        );
    }

}

CustomText.propTypes = {
    style: PropTypes.object,
    header: PropTypes.bool,
    header2: PropTypes.bool,
    header3: PropTypes.bool,
    infoText: PropTypes.bool,
    onPress: PropTypes.func,
    children: PropTypes.any
};

export default (withTheme(CustomText));