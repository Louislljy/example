import React from "react";
import { Image } from "react-native"

export default class Logo extends React.PureComponent {

    render() {
        return (
            <Image
                style={{ width: 60, height: 40, resizeMode: "contain" }}
                source={require('../../../assets/country.jpg')}
            />
        );
    }

}