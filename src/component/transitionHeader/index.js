import React from "react";
import { withTheme } from "react-native-paper";
import { View, StatusBar } from "react-native";
import CustomText from "../text"
import CustomIcon from "../icon"
import Animated from "react-native-reanimated"
const { Extrapolate, interpolate, color } = Animated

class TransitionHeader extends React.PureComponent {

    render() {
        let bgColor = color(255, 255, 255, interpolate(this.props.y, {
            inputRange: [0, this.props.heightTransition],
            outputRange: [0, 1],
            extrapolate: Extrapolate.CLAMP
        }))

        let bgColor2 = color(255, 255, 255, interpolate(this.props.y, {
            inputRange: [0, this.props.heightTransition],
            outputRange: [1, 0],
            extrapolate: Extrapolate.CLAMP
        }))

        return (
            <>
                <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />
                <Animated.View
                    style={{
                        height: 75,
                        position: "absolute",
                        top: 25,
                        width: "100%",
                        zIndex: 2,
                        backgroundColor: bgColor,
                        justifyContent: "space-between",
                        flexDirection: "row",
                    }}
                >
                    <View style={{ width: 55, margin: 10, }}>
                        <CustomIcon name="md-arrow-back" color={"white"} onPress={() => this.props.navigation.goBack()} />
                    </View>
                    <View style={{ margin: 10 }}>
                        <CustomText numberOfLines={1} style={{ color: "white" }} header2>{}</CustomText>
                    </View>
                    <View style={{ width: 55, margin: 10 }}>
                        <CustomText>{" "}</CustomText>
                    </View>
                </Animated.View>
                <Animated.View
                    style={{
                        height: 55,
                        position: "absolute",
                        top: 0,
                        width: "100%",
                        zIndex: 2,
                        opacity: bgColor2,
                        justifyContent: "space-between",
                        flexDirection: "row",

                    }}
                >
                    <View style={{ width: 55, margin: 10, }}>
                        <CustomIcon name="md-arrow-back" color={"black"} onPress={() => this.props.navigation.goBack()} />
                    </View>
                    <View style={{ margin: 10 }}>
                        <CustomText numberOfLines={1} style={{ color: "black" }} header2>{this.props.data.title}</CustomText>
                    </View>
                    <View style={{ width: 55, margin: 10 }}>
                        <CustomText>{" "}</CustomText>
                    </View>
                </Animated.View>
            </>
        );
    }

}

export default (withTheme(TransitionHeader));