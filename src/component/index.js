
import styled from "styled-components/native";
import theme from "../../theme";

import Bullet from "./bullet";
import CustomIcon from "./icon";
import Logo from "./logo";
import Loading from "./loading";

import CustomCarousel from "./carousel";
import Container from "./container";
import CustomButton from "./button";
import CustomText from "./text";
import CustomTextInput from "./textInput";
import CustomTabbar from "./customTabbar";

import OtpInput from "./otpInput";

import SubText from "./subtext";
import SideMenu from "./sideMenu";

import * as SlideButton from "./slideButton";
import TransitionHeader from "./transitionHeader";

const PaddedView = styled.View`
    padding: 10px;
`

const PaddedViewWithLine = styled.View`
    padding: 15px;
    padding-top: 30px;
    padding-Bottom: 30px;
    border-bottom-width: 0.5;
    border-bottom-color: ${theme.colors.greyText}

`
const TwoSideView = styled.View`
    flex: 1;
    flex-direction: row;
    margin-bottom: 2.5px;
    margin-top: 2.5px;
`

const TwoSideViewPolar = styled.View`
    flex: 1;
    flex-direction: row;
    margin-bottom: 2.5px;
    margin-top: 2.5px;
    justify-content: space-between;
`

export {
    Bullet,
    
    CustomCarousel,
    Container,
    CustomButton,
    CustomText,
    CustomTextInput,

    Logo,
    Loading,
    
    CustomIcon,
    CustomTabbar,

    OtpInput,
    
    PaddedView,
    PaddedViewWithLine,
    
    SubText,
    SideMenu,
    SlideButton,
    
    TwoSideView,
    TwoSideViewPolar,
    TransitionHeader,
}