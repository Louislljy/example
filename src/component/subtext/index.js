import React from "react";
import { withTheme } from "react-native-paper";
import CustomText from "../text";

class SubText extends React.PureComponent {

    render() {
        return (
            <CustomText
                {...this.props}
                style={{
                    color: this.props.theme.colors.greyText,
                    ...this.props.style,
                }}
            >
                {this.props.children}
            </CustomText>
        );
    }

}

export default withTheme(SubText);
