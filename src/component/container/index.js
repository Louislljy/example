import React from "react";
import styled from "styled-components/native";

const PaddedView = styled.View`
    flex: 1;
`;

const PaddedScrollView = styled.ScrollView`
    flex: 1;
`;

class Container extends React.PureComponent {

    renderView(canScroll) {
        if (canScroll) {
            return <PaddedScrollView showsVerticalScrollIndicator={false} {...this.props}
                style={{}}
                contentContainerStyle={{
                    ...this.props.style,
                    padding: this.props.padded ? 10 : null,
                    paddingBottom: this.props.tabVisible ? 50 : null,
                }}
            >
                {this.props.children}
            </PaddedScrollView>
        } else {
            return <PaddedView {...this.props}
                style={{
                    ...this.props.style,
                    padding: this.props.padded ? 10 : null,
                    paddingBottom: this.props.tabVisible ? 50 : null,
                }}
            >
                {this.props.children}
            </PaddedView>
        }
    }

    render() {
        return this.renderView(this.props.scrollable);
    }

}

export default Container;