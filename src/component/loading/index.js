import React from "react";
import LottieView from "lottie-react-native";
import { Dimensions, View } from 'react-native';
const { width: viewportWidth } = Dimensions.get('window');

export default class Loading extends React.PureComponent {

    type(type) {
        if (type == "page") {
            return require("../../../assets/loading.json");
        } else if (type == "element") {
            return require("../../../assets/loaddot.json")
        } else {
            return require("../../../assets/loading.json");
        }
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <LottieView style={{ height: viewportWidth / 2, width: viewportWidth / 2 }} source={this.type(this.props.type)} autoPlay loop />
            </View>
        );
    }

}