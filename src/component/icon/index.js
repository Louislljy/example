import React from "react";
import { TouchableOpacity } from "react-native";
import { Button } from "react-native-paper";
import Icon from "react-native-vector-icons/Ionicons";

export default class CustomIcon extends React.PureComponent {

    render() {
        return (
            <Button
                onPress={this.props.onPress}
                icon={({ size, color }) => (
                    <TouchableOpacity
                        onPress={this.props.onPress}
                    >
                        <Icon
                            name={this.props.name}
                            style={{ fontSize: 25 }}
                            color={this.props.color ? this.props.color : (this.props.white ? "white" : "black")}
                        />
                    </TouchableOpacity>

                )}
            />
        );
    }

}