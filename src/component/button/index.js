import React from "react";
import { Button } from "react-native-paper";
import { CustomText } from "../text";

class CustomButton extends React.PureComponent {

    render() {
        return (
            <Button
                {...this.props}
                style={{
                    ...this.props.style,
                    paddingTop: 5,
                    paddingBottom: 5,
                }}
            >
                {
                    this.props.text
                        ? <CustomText>{this.props.text}</CustomText>
                        : this.props.children
                }
            </Button>
        );
    }
}

export default CustomButton;