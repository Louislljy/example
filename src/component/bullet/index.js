import React from "react";
import { withTheme } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import { CustomText } from "../text";

export default withTheme(class Bullet extends React.PureComponent {

    render() {
        return (
            <View style={styles.row}>
                <View style={styles.bullet}>
                    <CustomText>{this.props.index + ". "}</CustomText>
                </View>
                <View style={styles.bulletText}>
                    <CustomText>
                        <CustomText style={styles.normalText}>{this.props.text}</CustomText>
                    </CustomText>
                </View>
            </View>
        );
    }

})

var styles = StyleSheet.create({
    // column: {
    //     flexDirection: 'column',
    //     alignItems: 'flex-start',
    // },
    row: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        flex: null,
        marginTop: 10,
        marginBottom: 5
    },
    bullet: {
        width: 20
    },
    bulletText: {
        flex: 1
    },
    boldText: {
        fontWeight: 'bold'
    },
    normalText: {
    }
});